#! /usr/bin/env python3

from flask import Flask, request

import wit
import truucs
import hotword
import ircbot

app = Flask(__name__)

@app.route("/googlenow", methods=["POST"])
def googlenow():
    text = request.form["text"]
    print("Text:", text)

    intent, params = wit.get_intent(text)
    k, v = max(params.items(), key=lambda p: max(c["confidence"] for c in p[1]))
    params = {k: v}
    truucs.dispatch(intent, params)

    return intent

@app.route("/action", methods=["POST"])
def action():
    if not "action" in request.form:
        return "non."
    act = request.form["action"]
    if act == "light_on":
        truucs.truucs["light"].set_light("on")
    elif act == "light_off":
        truucs.truucs["light"].set_light("off")
    elif act == "garage":
        truucs.truucs["garage"].set_garage("whatever")
    else:
        "J'ai pas compris."

@app.route("/")
def webpage():
    html = ""
    with open("luucmiere.html") as f:
        html = f.read()
    return html

if __name__ == "__main__":
    hw = hotword.Hotword()
    hw.listen_async()
    app.run(host="0.0.0.0")
