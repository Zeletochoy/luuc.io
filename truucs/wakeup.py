from config import *
from . import Truuc
import requests

class Wakeup(Truuc):
    def je_que_tu_mon_intent(self, params):
        t = params["time"][0]["value"][:16]
        requests.post(LIGHT_URL + "/wakeup", data={"time": t})


    def en_fait_non(self):
        requests.post(LIGHT_URL + "/wakeup", data={"cancel": True})
