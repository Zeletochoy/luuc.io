from os.path import dirname, basename, isfile
import glob
import sys
import inspect
import importlib

class Truuc:
    def je_que_tu_mon_intent(self, params):
        pass

    def en_fait_non(self):
        pass

truucs = {}

for f in glob.glob(dirname(__file__)+"/*.py"):
    if isfile(f) and not f[0] == '_':
        name = basename(f)[:-3]
        mod = importlib.import_module("." + name, "truucs")
        for cname, cls in mod.__dict__.items():
            try:
                if type(cls) == type(Truuc) and cls != Truuc and issubclass(cls, Truuc):
                    truucs[cname.lower()] = cls()
            except Exception as e:
                import pdb; pdb.set_trace()
                print(e)
        del mod


def dispatch(intent, params):
    if intent.lower() == "cancel" and dispatch.last_truuc is not None:
        dispatch.last_truuc.en_fait_non()
    else:
        try:
            truuc = truucs[intent.lower()]
            dispatch.last_truuc = truuc
            truuc.je_que_tu_mon_intent(params)
        except Exception as e:
            print("Dispatch:", e)
dispatch.last_truuc = None
