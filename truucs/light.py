from config import *
from . import Truuc
import requests

class Light(Truuc):
    def __init__(self):
        self.state = self.get_state()
        self.old_state = self.state

    def get_state(self):
        try:
            res = requests.get(LIGHT_URL + "/state")
            return (res.content.decode("ascii") == "on")
        except Exception as e:
            print("Light:", e)

    def set_light(self, state):
        try:
            res = requests.get(LIGHT_URL + "/" + state)
            print("Light returned:", res.status_code, res.content.decode("ascii"))
            if res.status_code == 200:
                self.old_state = self.state
                self.state = (state == "on")
        except Exception as e:
            print("Light:", e)

    def je_que_tu_mon_intent(self, params):
        print(params)
        new_state = params["powerstate"][0]["value"]
        if new_state not in ("on", "off"):
            print("Light: Invalid state", new_state)
        else:
            self.set_light(new_state)

    def en_fait_non(self):
        self.set_light("on" if self.old_state else "off")
