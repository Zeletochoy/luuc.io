from config import *
from . import Truuc
from gmusicapi import Mobileclient
from getpass import getpass
import subprocess
import threading
import time

class Music(Truuc):
    def __init__(self):
        password = "".join(chr(ord(c)-9) for c in PLAYMUSIC_PASS) # Trv3 5e(ur1ty
        self.client = Mobileclient();
        if not self.client.login(PLAYMUSIC_LOGIN, password, Mobileclient.FROM_MAC_ADDRESS):
            print("Google Play Music: Login failed")
            return
        self.player = None
        self.playlist = []
        def play_list(self):
            while True:
                self.stop_player()
                for song_id in self.playlist:
                    print("Playing song id", song_id)
                    url = self.client.get_stream_url(song_id)
                    self.player = subprocess.Popen(["cvlc", "--play-and-exit", url])
                    self.player.wait()
                    if len(self.playlist) == 0:
                        break
                time.sleep(0.1)
        self.player_thread = threading.Thread(target=play_list, args=(self,))
        self.player_thread.start()

    def __del__(self):
        self.client.logout()

    def get_state(self):
        return False #FIXME

    def stop_player(self):
        if self.player is not None:
            self.player.terminate()
            self.player = None

    def play_song(self, title):
        print("Searching for", title)
        res = self.client.search(title, 1)
        print(res)
        song_id = res["song_hits"][0]["track"]["nid"]
        self.playlist.append(song_id)

    def play_artist(self, title):
        print("Searching for", title)
        res = self.client.search(title, 1)
        print(res)
        artist_id = res["artist_hits"][0]["artist"]["artistId"]
        res = self.client.get_artist_info(artist_id, max_top_tracks=1000)
        song_ids = [s["nid"] for s in res["topTracks"]]
        print("Playlist for {}:".format(res["name"]))
        for s in res["topTracks"]:
            print('-', s["title"])
        self.playlist += song_ids

    def change_vol(self, percent):
        sign = '+' if percent >= 0 else ''
        subprocess.Popen(["pactl", "set-sink-volume", "0", "--", sign+str(percent)+'%'])

    def je_que_tu_mon_intent(self, params):
        print(params)
        if "song" in params:
            title = params["song"][0]["value"]
            self.play_song(title)
        elif "group" in params:
            artist = params["group"][0]["value"]
            self.play_artist(artist)
        elif "control" in params:
            action = params["control"][0]["value"]
            if action == "next":
                self.stop_player()
            elif action == "vol_up":
                self.change_vol(3)
            elif action == "vol_down":
                self.change_vol(-3)
        else:
            print("Unknown params:", params)

    def en_fait_non(self):
        self.playlist = []
        self.stop_player()
