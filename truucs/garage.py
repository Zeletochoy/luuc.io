from config import *
from . import Truuc
import requests

class Garage(Truuc):
    def __init__(self):
        self.state = self.get_state()
        self.old_state = self.state

    def get_state(self):
        return False #FIXME
        #try:
        #    res = requests.get(garage_URL + "/state")
        #    return (res.content.decode("ascii") == "on")
        #except Exception as e:
        #    print("Garage:", e)

    def set_garage(self, state):
        #if state == self.state:
        #    print("Garage: no action needed")
        #    return
        try:
            res = requests.post(GARAGE_URL, params={"access_token": GARAGE_TOKEN})
            print("Garage returned:", res.status_code, res.content.decode("ascii"))
            if res.status_code == 200:
                self.old_state = self.state
                self.state = state
        except Exception as e:
            print("Garage:", e)

    def je_que_tu_mon_intent(self, params):
        print(params)
        new_state = params["garage"][0]["value"]
        if new_state not in ("open", "close"):
            print("Garage: Invalid state", new_state)
        else:
            self.set_garage(new_state == "open")

    def en_fait_non(self):
        self.set_garage(self.old_state)
