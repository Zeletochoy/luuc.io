from config import *
import requests
from urllib.parse import quote as urlencode

def get_intent(text):
    r = requests.get(WIT_URL + urlencode(text),
                     headers={"Authorization": "Bearer " + WIT_TOKEN})
    r = r.json()

    intent = r["outcomes"][0]["intent"]
    params = r["outcomes"][0]["entities"]

    return intent, params
