#! /usr/bin/env python3

import pyaudio
import struct
from scipy import fftpack
import numpy as np
import threading
import itertools
from collections import deque
from sklearn.externals import joblib
import subprocess
import wave
import io
import requests

import truucs
from config import *

class Hotword:
    delta = 8192 # about 1/5sec
    samples_per_fft = 5 # about 1sec
    sample_rate = 44100 
    hanning = np.hanning(delta * samples_per_fft)
    mic = pyaudio.PyAudio()

    def __init__(self):
        self.mic_stream = self.mic.open(format=pyaudio.paInt16, channels=1,
                rate=self.sample_rate, input=True, frames_per_buffer=self.delta)
        self.pca = joblib.load("pca.pkl")
        self.classifier = joblib.load("classifier.pkl")

    def mic_gen(self):
        while True:
            data = self.mic_stream.read(self.delta)
            data = np.fromstring(data, dtype=np.int16)
            yield data

    def current_fft(self):
        sample = np.concatenate(self.buf)
        res = fftpack.rfft(sample * self.hanning) / self.sample_rate
        return res

    def is_luuc(self, fft):
        data = self.pca.transform([fft])
        return self.classifier.predict(data)[0] == 1

    def snd_to_intent(self, snd):
        #import pdb; pdb.set_trace()
        headers = { 'Content-Type': 'audio/wav',
                    'Authorization': 'Bearer ' + WIT_TOKEN}
        data = None
        with io.BytesIO() as wav_file:
            with wave.open(wav_file, "wb") as wav_writer:
                wav_writer.setframerate(self.sample_rate)
                wav_writer.setsampwidth(2)
                wav_writer.setnchannels(1)
                wav_writer.writeframes(snd)
            data = wav_file.getvalue()
        r = requests.post(WIT_URL, headers=headers, data=data)
        print(r.text)
        r = r.json()
        intent = r["outcomes"][0]["intent"]
        params = r["outcomes"][0]["entities"]
        return intent, params

    def listen_command(self):
        snd = []
        silence_time = 5
        for _ in range(7): # don't listen to ouiii
            next(self.mic_gen())
        beginning = itertools.islice(self.mic_gen(), silence_time)
        silence_buf = deque(beginning, silence_time)
        while any(np.mean(s) > 3 for s in silence_buf):
            print([np.mean(s) for s in silence_buf])
            snd.append(silence_buf.popleft())
            silence_buf.append(next(self.mic_gen()))
        return np.concatenate(snd)

    def listen(self):
        beginning = itertools.islice(self.mic_gen(), self.samples_per_fft)
        self.buf = deque(beginning, self.samples_per_fft)
        while True:
            fft = self.current_fft()
            freqs = fftpack.rfftfreq(len(fft), 1/self.sample_rate)
            f = freqs[np.argmax(np.abs(fft))]
            if self.is_luuc(fft):
                subprocess.check_output(["cvlc", "--play-and-exit", "ouii.mp3"])
                snd = self.listen_command()
                intent, params = self.snd_to_intent(snd) 
                truucs.dispatch(intent, params)
                beginning = itertools.islice(self.mic_gen(), self.samples_per_fft)
                self.buf = deque(beginning, self.samples_per_fft)
            else:
                self.buf.append(next(self.mic_gen()))

    def listen_async(self):
        threading.Thread(target=self.listen).run()

if __name__ == "__main__":
    hw = Hotword()
    hw.listen()
