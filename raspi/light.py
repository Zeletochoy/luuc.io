#! /usr/bin/env python

from flask import Flask, request
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
import RPi.GPIO as gpio

gpio.setmode(gpio.BOARD)
gpio.setup(3, gpio.OUT)
gpio.output(3, False)
state = False

app = Flask(__name__)

def set_lamp(new_state):
    global state
    gpio.output(3, new_state)
    state = new_state
    print "Turning the lamp " + ("on" if new_state else "off")

@app.route("/off")
def power_off():
    set_lamp(False)
    return "off"

@app.route("/on")
def power_on():
    set_lamp(True)
    return "on"

@app.route("/state")
def state():
    print "on" if state else "off"
    return "on" if state else "off"

sched = BackgroundScheduler()
sched.start()

@app.route("/wakeup", methods=["POST"])
def wakeup():
    if "cancel" in request.form and wakeup.job is not None:
        wakeup.job.remove()
        wakeup.job = None
        print("Deactivated the alarm")
        return
    t = datetime.strptime(request.form["time"], "%Y-%m-%dT%H:%M")
    wakeup.job = sched.add_job(lambda: set_lamp(True), next_run_time=t)
    print("Scheduling light for", t)
wakeup.job = None

if __name__ == "__main__":
    app.run(host="0.0.0.0")
